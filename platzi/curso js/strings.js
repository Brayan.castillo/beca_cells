/* Clase 1 */
/*  var nombre = 'sacha', apellido = 'Castillo';

 var nombreEnMayusculas = nombre.toUpperCase();
 var apellidoEnMinusculas = apellido.toLowerCase();

 var preimeraLetraDelNombre = nombre.charAt(0);
 var cantidadDeLetrasDelNombre = nombre.length;

/*  var nombreCompleto = nombre + " " + apellido; */
/* otra forma de concatenar o tambien escribir
 codigo de javaScript es de la siguiente manera*/

/*  var nombreCompleto = `${nombre} ${apellido.toUpperCase()}`; */

/* var str = nombre.charAt(1) + nombre.charAt(2); 
otra forma de ponerlo es la siguiente*/

/* var str = nombre.substr(1,3); */

/* var ultimaLetraDeTuNombre = nombre.charAt( ); */

/* Clase 2 */
/* var edad  = 27;

edad += 1;

var peso = 75;

peso -= 2;

var sandwich = 1;

peso += sandwich;

var jugarAlFutbol = 3;

peso -= jugarAlFutbol;

var precioDeVino = 200.3

var total = Math.round(precioDeVino * 100 * 3)/100;
var totalStr = total.toFixed(2);
var total2 = parseFloat(totalStr);

var pizza = 8 
var persona = 2
var cantidadPorcionesPorPersona = pizza / persona */

/* Clase 3 Funciones*/

/* var nombre = 'sasha'
var edad = 28

function imprimirEdad(nombre , edad){
    console.log(`${nombre} tiene ${edad} año`)
}

imprimirEdad(nombre , edad);

imprimirEdad('Vicky', 28)
imprimirEdad('eric', 24)
imprimirEdad('dario', 27)
imprimirEdad(25, 'carls')
imprimirEdad()
 */

 /* Clase 4 alcance de las funciones */
/*  var nombre = 'sacha'

 function imprimirMayusculas(n){
     n = n.toUpperCase()
     console.log(n)
 }

 imprimirMayusculas(nombre) */

 /* clase 5 objetos */

/*  var sacha = {
     nombre: 'sacha',
     apellido: 'Castillo',
     edad: 28
 }

 var brayan = {
     nombre: 'brayan',
     apellido: 'castillo',
     edad: 23
 }


 function imprimirMayusculas({ nombre }){
     console.log(nombre.toUpperCase())
 }

 function cualEsTuEdad({ nombre, edad}){
     console.log(nombre, "tu edad es", edad)
 }

 imprimirMayusculas(sacha)
cualEsTuEdad(brayan) */

/* Clase 6 Desestructurar objetos */

/*  var sacha = {
     nombre: 'sacha',
     apellido: 'Castillo',
     edad: 28
 }

 var brayan = {
     nombre: 'brayan',
     apellido: 'castillo',
     edad: 23
 }


 function imprimirMayusculas({ nombre }){
     console.log(nombre.toUpperCase())
 }

 function cualEsTuEdad({ nombre, edad}){
     console.log("Hola mi nombre es ", nombre.toUpperCase() , "y tengo " , edad , "años")
 }

 imprimirMayusculas(sacha)
cualEsTuEdad(brayan)
cualEsTuEdad(sacha) */

/* clase 7 Parametros como referencia o valor */

/* var sacha = {
     nombre: 'sacha',
     apellido: 'Castillo',
     edad: 28
 }

 var brayan = {
     nombre: 'brayan',
     apellido: 'castillo',
     edad: 23
 }


 function imprimirMayusculas({ nombre }){
     console.log(nombre.toUpperCase())
 }

 function cualEsTuEdad({ nombre, edad}){
     console.log("Hola mi nombre es ", nombre.toUpperCase() , "y tengo " , edad , "años")
 }

imprimirMayusculas(sacha)
cualEsTuEdad(brayan)
cualEsTuEdad(sacha)

function cumpleAneos (persona){
    return{
        ...persona,
        edad: persona.edad + 1
    }
} */

/* Clase 8 comparaciones en JavaScript */

/* var x = 4, y = '4'

var sacha = {
   nombre: 'Sacha'
}
var otraPersona  = {
   nombre: 'Sacha'
} */

/* Clase 9 condicionales */

/* var brayan = {
    nombre: 'brayan',
    apellido: 'castillo',
    edad: 23,
    ingeniero: true,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
}

function imprimirProfeciones(persona){
    console.log(`${persona.nombre} es:`)

    if(persona.ingeniero){
        console.log('ingeniero')
    }
    if(persona.cocinero){
        console.log('cocinero')
    }
    if(persona.cantante){
        console.log('cantante')
    }
    if(persona.dj){
        console.log('dj')
    }
    if(persona.guitarrista){
        console.log('guitarrista')
    }
    if(persona.drone){
        console.log('piloto de drone')
    }
}

imprimirProfeciones(brayan)


function imprimirSiesMayordeedad(persona){
    console.log(`${persona.nombre} es:`)
    
    if(persona.edad >= 18 ){
        console.log(`mayor de edad`)
    }else{
        console.log(`menor de edad`)
    }
}

imprimirSiesMayordeedad(brayan) */

/* Clase 10 funciones que retornan valores */

/* var brayan = {
    nombre: 'brayan',
    apellido: 'castillo',
    edad: 23,
    ingeniero: true,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
}

var juan = {
    nombre: 'juan',
    apellido: 'peres',
    edad: 13
}

const MAYORIA_DE_EDAD = 18

function esMayorDeEdad (persona){
    return persona.edad >= MAYORIA_DE_EDAD
}

function imprimirSiesMayordeedad(persona){
    if(esMayorDeEdad(persona) ){
        console.log(`${persona.nombre} es mayor de edad`)
    }else{
        console.log(`${persona.nombre} es menor de edad`)
    }
}

imprimirSiesMayordeedad(brayan)
imprimirSiesMayordeedad(juan) */

/* Clase 11 arrow functions */

/* var brayan = {
    nombre: 'brayan',
    apellido: 'castillo',
    edad: 23,
    ingeniero: true,
    cocinero: false,
    cantante: false,
    dj: false,
    guitarrista: false,
    drone: true
}

var juan = {
    nombre: 'juan',
    apellido: 'peres',
    edad: 13
}

const MAYORIA_DE_EDAD = 18

function esMayorDeEdad (persona){
    return persona.edad >= MAYORIA_DE_EDAD
} 
const esMayorDeEdad = ({edad}) => edad >= MAYORIA_DE_EDAD

function imprimirSiesMayordeedad(persona){
    if(esMayorDeEdad(persona) ){
        console.log(`${persona.nombre} es mayor de edad`)
    }else{
        console.log(`${persona.nombre} es menor de edad`)
    }
}

function personaConAcceso (persona){
    if(!esMayorDeEdad(persona)){
        console.log(`${persona.nombre} acceso denegado`)
    }
}


imprimirSiesMayordeedad(brayan)
imprimirSiesMayordeedad(juan) */

/* Clase 12 arrays */

/* var brayan = {
    nombre: 'brayan',
    apellido: 'castillo',
    altura: 1.70
}
var nicolas = {
    nombre: 'nicolas',
    apellido: 'reyes',
    altura: 1.68
}
var Elias = {
    nombre: 'Elias',
    apellido: 'santana',
    altura: 1.75
}
var Oscar = {
    nombre: 'Oscar',
    apellido: 'Ramires',
    altura: 1.90
}
var yadira = {
    nombre: 'yadira',
    apellido: 'Rodrigues',
    altura: 1.56
}
var armando = {
    nombre: 'armando',
    apellido: 'figueroa',
    altura: 1.73
}

var personas = [brayan, nicolas, Elias, Oscar, yadira, armando]

for (var i = 0; i < personas.length; i++){
    var persona = personas [i]
    console.log(`${persona.nombre} mide ${persona.altura}mts`)
} */

/* Clase 13 filtrar elementos de un arrauy */

/* var brayan = {
    nombre: 'brayan',
    apellido: 'castillo',
    altura: 1.70
}
var nicolas = {
    nombre: 'nicolas',
    apellido: 'reyes',
    altura: 1.68
}
var Elias = {
    nombre: 'Elias',
    apellido: 'santana',
    altura: 1.75
}
var Oscar = {
    nombre: 'Oscar',
    apellido: 'Ramires',
    altura: 1.90
}
var yadira = {
    nombre: 'yadira',
    apellido: 'Rodrigues',
    altura: 1.56
}
var armando = {
    nombre: 'armando',
    apellido: 'figueroa',
    altura: 1.73
}

const esAlta = ({altura}) => altura < 1.8

var personas = [brayan, nicolas, Elias, Oscar, yadira, armando]


var personasAltas = personas.filter(esAlta)

console.log(personasAltas) */

/* Clase 14 Transformar un array */

/* var brayan = {
    nombre: 'brayan',
    apellido: 'castillo',
    altura: 1.70
}
var nicolas = {
    nombre: 'nicolas',
    apellido: 'reyes',
    altura: 1.68
}
var Elias = {
    nombre: 'Elias',
    apellido: 'santana',
    altura: 1.75
}
var Oscar = {
    nombre: 'Oscar',
    apellido: 'Ramires',
    altura: 1.90
}
var yadira = {
    nombre: 'yadira',
    apellido: 'Rodrigues',
    altura: 1.56
}
var armando = {
    nombre: 'armando',
    apellido: 'figueroa',
    altura: 1.73
}

const esAlta = ({altura}) => altura > 1.8

var personas = [brayan, nicolas, Elias, Oscar, yadira, armando]

var personasAltas = personas.filter(esAlta)

const pasarAlturaaCsm = persona => ({
    ...persona,
    altura: persona.altura * 100
})
var personasCsm = personas.map(pasarAlturaaCsm)


console.log(personasAltas) 
console.log(personasCsm) */

/* Clase 15 como funcionan las clases en javaScript */

/* function persona (nombre, apellido) {
    this.nombre = nombre
    this.apellido = apellido
}

persona.prototype.saludar = function (){
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
}

var arturo = new persona('Arturo' ,'medina')
var erika = new persona('Erika', 'Luna')
var brayan = new persona('brayan', 'castillo')
brayan.saludar()
 */

 /* Clase 16 modificando un prototipo */

/* 
function persona (nombre, apellido, altura) {
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}

persona.prototype.saludar = function (){
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
}

persona.prototype.soyAlto = function(){
    return this.altura > 1.8
}

var arturo = new persona('Arturo' ,'medina', 1.72)
var erika = new persona('Erika', 'Luna', 1.65)
var brayan = new persona('brayan', 'castillo', 1.89) */

/* Clase 17 La verdad oculta sobre las clases en javaScript */

/* function persona (nombre, apellido, altura) {
    this.nombre = nombre
    this.apellido = apellido
    this.altura = altura
}
function heredaDe(prototipoHijo, prototipoPadre){
    var fn = function (){}
    fn.prototype = prototipoPadre.prototype
    prototipoHijo.prototype = new fn
    prototipoHijo.prototype.constructor = prototipoHijo
}

heredaDe(Desarrollador, persona)

persona.prototype.saludar = function (){
    console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
}

persona.prototype.soyAlto = function(){
    return this.altura > 1.8
}


var arturo = new persona('Arturo' ,'medina', 1.72)
var erika = new persona('Erika', 'Luna', 1.65)
var brayan = new persona('brayan', 'castillo', 1.89) */

/* Clase 18 clases en javaScript */

/* class persona {
    constructor(nombre, apellido, altura) {
        this.nombre = nombre
        this.apellido = apellido
        this.altura = altura
    }
}
    saludar() {
        console.log(`Hola, me llamo ${this.nombre} ${this.apellido}`)
    }
    soyAlto() {
        return this.altura > 1.8
    }

    class Desarrollador extends persona {
        constructor(nombre, apellido, altura) {
           super(nombre,apellido,altura)
        }
    }

 saludar() {
     console.log(`Hola, me llamo ${this.nombre} ${this.apellido} y soy desarrollador/a`)
 } */

 